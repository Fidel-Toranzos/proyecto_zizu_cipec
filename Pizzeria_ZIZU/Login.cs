﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;

using BDSYSTEM;
using Control;



namespace Pizzeria_ZIZU
{
    public partial class Login : Form
    {
        Verification proc = new Verification();
        ConectionBD cox = new ConectionBD();
        public Login()
        {
            InitializeComponent();
        }


        private void btnIngresar_Click_1(object sender, EventArgs e)
        {
            try
            {
                if(txtUser.Text.Equals("") && txtPassword.Text.Equals(""))
                {
                    MessageBox.Show("Debe llenar los datos, ingrese su cuenta de usuario");
                    txtUser.Text = null;
                    txtPassword.Text = null;
                    txtUser.Focus();
                }
                else
                {
                    
                    if (proc.IngresarUserPassword(txtUser.Text, txtPassword.Text) == 1)
                    {
                        cox.enlaceBD().Close();
                        MessageBox.Show("Bienvenido a Pizza ZIZU");
                        Loading_Bar cargar = new Loading_Bar();
                        this.Visible = false;
                        cargar.Show();
                    }
                    else
                    {
                        MessageBox.Show("Datos incorrectas, ingrese su cuenta de usuario");
                        txtUser.Text = null;
                        txtPassword.Text = null;
                        txtUser.Focus();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void txtUser_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void txtPassword_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void btnCerrar_Click_1(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void Login_Load(object sender, EventArgs e)
        {

        }
    }
}
