﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Data.SqlClient;
using System.Data.Sql;

using Control;
using BDSYSTEM;

namespace Pizzeria_ZIZU
{
    public partial class frmPizza : Form
    {
        public string tam1 = "", sabs1 = "", sabone1 = "", sabtwo1 = "", precio1 = "", precioFabri1 = "", borde1 = "";
        public string tam2 = "", sabs2 = "", sabone2 = "", sabtwo2 = "", precio2 = "", precioFabri2 = "", borde2 = "";
        public string tam3 = "", sabs3 = "", sabone3 = "", sabtwo3 = "", precio3 = "", precioFabri3 = "", borde3 = "";
        public string tam4 = "", sabs4 = "", sabone4 = "", sabtwo4 = "", precio4 = "", precioFabri4 = "", borde4 = "";
        public string tam5 = "", sabs5 = "", sabone5 = "", sabtwo5 = "", precio5 = "", precioFabri5 = "", borde5 = "";
        public double total = 0;
        ventaProceso regist = new ventaProceso();
        ConectionBD contbd = new ConectionBD();
        public bool res = false;
        public frmPizza()
        {
            InitializeComponent();
            //centra la ventana 
            this.StartPosition = FormStartPosition.CenterScreen;
            cbsabor1.Enabled = false;
            cbsabor2.Enabled = false;
        }

        private void frmPizza_Load(object sender, EventArgs e)
        {
            cbTamano1.Items.Add("XP-(2P)");
            cbTamano1.Items.Add("P-(4P)");
            cbTamano1.Items.Add("M-(6P)");
            cbTamano1.Items.Add("G-(8P)");
            cbTamano1.Items.Add("XG-(12P)");
            rbsabor1.Checked = false;
            radioButton1.Checked = false;
            cbsabor1.Items.Clear();
            cbsabor2.Items.Clear();
            cbsabor1.Enabled = false;
            cbsabor2.Enabled = false;
            rbno.Checked = true;
        }

        private void button1_Click(object sender, EventArgs e) // registrar pedido por orden
        {
            Menu_Orde most = new Menu_Orde();
            if (cbTamano1.Text != "")
            {
                if (rbsabor1.Checked == true && radioButton1.Checked == false)
                {
                    if (cbsabor1.Text != "")
                    {
                        if (titulopiz.Text == "1")
                        {
                            tam1 = regist.guardarDato(cbTamano1.Text);
                            sabs1 = regist.guardarDato("Uno");
                            sabone1 = regist.guardarDato(cbsabor1.Text);
                            sabtwo1 = regist.guardarDato("No");
                            precio1 = regist.enviarPrecio(tam1, sabone1);
                            total = total + Convert.ToDouble(precio1);
                            precioFabri1 = regist.enviarFabricacion(tam1, sabone1);
                            if (rbyes.Checked == true)
                            {
                                total = total + 10;
                                borde1 = regist.guardarDato("Si");
                            }
                            else
                            {
                                borde1 = regist.guardarDato("No");
                            }
                        }
                        else if (titulopiz.Text == "2")
                        {
                            tam2 = regist.guardarDato(cbTamano1.Text);
                            sabs2 = regist.guardarDato("Uno");
                            sabone2 = regist.guardarDato(cbsabor1.Text);
                            sabtwo2 = regist.guardarDato("No");
                            precio2 = regist.enviarPrecio(tam2, sabone2);
                            total = total + Convert.ToDouble(precio2);
                            precioFabri2 = regist.enviarFabricacion(tam2, sabone2);
                            if (rbyes.Checked == true)
                            {
                                total = total + 10;
                                borde2 = regist.guardarDato("Si");
                            }
                            else
                            {
                                borde2 = regist.guardarDato("No");
                            }
                        }
                        else if (titulopiz.Text == "3")
                        {
                            tam3 = regist.guardarDato(cbTamano1.Text);
                            sabs3 = regist.guardarDato("Uno");
                            sabone3 = regist.guardarDato(cbsabor1.Text);
                            sabtwo3 = regist.guardarDato("No");
                            precio3 = regist.enviarPrecio(tam3, sabone3);
                            total = total + Convert.ToDouble(precio3);
                            precioFabri3 = regist.enviarFabricacion(tam3, sabone3);
                            if (rbyes.Checked == true)
                            {
                                total = total + 10;
                                borde3 = regist.guardarDato("Si");
                            }
                            else
                            {
                                borde3 = regist.guardarDato("No");
                            }
                        }
                        else if (titulopiz.Text == "4")
                        {
                            tam4 = regist.guardarDato(cbTamano1.Text);
                            sabs4 = regist.guardarDato("Uno");
                            sabone4 = regist.guardarDato(cbsabor1.Text);
                            sabtwo4 = regist.guardarDato("No");
                            precio4 = regist.enviarPrecio(tam4, sabone4);
                            total = total + Convert.ToDouble(precio4);
                            precioFabri4 = regist.enviarFabricacion(tam4, sabone4);
                            if (rbyes.Checked == true)
                            {
                                total = total + 10;
                                borde4 = regist.guardarDato("Si");
                            }
                            else
                            {
                                borde4 = regist.guardarDato("No");
                            }
                        }
                        else
                        {
                            tam5 = regist.guardarDato(cbTamano1.Text);
                            sabs5 = regist.guardarDato("Uno");
                            sabone5 = regist.guardarDato(cbsabor1.Text);
                            sabtwo5 = regist.guardarDato("No");
                            precio5 = regist.enviarPrecio(tam5, sabone5);
                            total = total + Convert.ToDouble(precio5);
                            precioFabri5 = regist.enviarFabricacion(tam5, sabone5);
                            if (rbyes.Checked == true)
                            {
                                total = total + 10;
                                borde5 = regist.guardarDato("Si");
                            }
                            else
                            {
                                borde5 = regist.guardarDato("No");
                            }
                        }
                        cbTamano1.Items.Clear();
                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show("Ingrese el sabor de la pizza");
                    }
                }
                else if (rbsabor1.Checked == false && radioButton1.Checked == true)
                {
                    if (cbsabor1.Text != "" && cbsabor2.Text != "")
                    {
                        if (titulopiz.Text == "1")
                        {
                            tam1 = regist.guardarDato(cbTamano1.Text);
                            sabs1 = regist.guardarDato("Dos");
                            sabone1 = regist.guardarDato(cbsabor1.Text);
                            sabtwo1 = regist.guardarDato(cbsabor2.Text);
                            precio1 = regist.enviarPrecio(tam1, sabone1);
                            total = total + Convert.ToDouble(precio1);
                            precioFabri1 = regist.enviarFabricacion(tam1, sabone1);
                            if (rbyes.Checked == true)
                            {
                                total = total + 10;
                                borde1 = regist.guardarDato("Si");
                            }
                            else
                            {
                                borde1 = regist.guardarDato("No");
                            }
                        }
                        else if (titulopiz.Text == "2")
                        {
                            tam2 = regist.guardarDato(cbTamano1.Text);
                            sabs2 = regist.guardarDato("Dos");
                            sabone2 = regist.guardarDato(cbsabor1.Text);
                            sabtwo2 = regist.guardarDato(cbsabor2.Text);
                            precio2 = regist.enviarPrecio(tam2, sabone2);
                            total = total + Convert.ToDouble(precio2);
                            precioFabri2 = regist.enviarFabricacion(tam2, sabone2);
                            if (rbyes.Checked == true)
                            {
                                total = total + 10;
                                borde2 = regist.guardarDato("Si");
                            }
                            else
                            {
                                borde2 = regist.guardarDato("No");
                            }
                        }
                        else if (titulopiz.Text == "3")
                        {
                            tam3 = regist.guardarDato(cbTamano1.Text);
                            sabs3 = regist.guardarDato("Dos");
                            sabone3 = regist.guardarDato(cbsabor1.Text);
                            sabtwo3 = regist.guardarDato(cbsabor2.Text);
                            precio3 = regist.enviarPrecio(tam3, sabone3);
                            total = total + Convert.ToDouble(precio3);
                            precioFabri3 = regist.enviarFabricacion(tam3, sabone3);
                            if (rbyes.Checked == true)
                            {
                                total = total + 10;
                                borde3 = regist.guardarDato("Si");
                            }
                            else
                            {
                                borde3 = regist.guardarDato("No");
                            }
                        }
                        else if (titulopiz.Text == "4")
                        {
                            tam4 = regist.guardarDato(cbTamano1.Text);
                            sabs4 = regist.guardarDato("Dos");
                            sabone4 = regist.guardarDato(cbsabor1.Text);
                            sabtwo4 = regist.guardarDato(cbsabor2.Text);
                            precio4 = regist.enviarPrecio(tam4, sabone4);
                            total = total + Convert.ToDouble(precio4);
                            precioFabri4 = regist.enviarFabricacion(tam4, sabone4);
                            if (rbyes.Checked == true)
                            {
                                total = total + 10;
                                borde4 = regist.guardarDato("Si");
                            }
                            else
                            {
                                borde4 = regist.guardarDato("No");
                            }
                        }
                        else
                        {
                            tam5 = regist.guardarDato(cbTamano1.Text);
                            sabs5 = regist.guardarDato("Dos");
                            sabone5 = regist.guardarDato(cbsabor1.Text);
                            sabtwo5 = regist.guardarDato(cbsabor2.Text);
                            precio5 = regist.enviarPrecio(tam5, sabone5);
                            total = total + Convert.ToDouble(precio5);
                            precioFabri5 = regist.enviarFabricacion(tam5, sabone5);
                            if (rbyes.Checked == true)
                            {
                                total = total + 10;
                                borde5 = regist.guardarDato("Si");
                            }
                            else
                            {
                                borde5 = regist.guardarDato("No");
                            }
                        }
                        cbTamano1.Items.Clear();
                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show("Ingrese los sabores de la pizza");
                    }
                }
                else
                {
                    MessageBox.Show("Ingrese la cantidad de sabor de la pizza");
                }
            }
            else
            {
                MessageBox.Show("Llene el campo del tamaño de la pizza");
            }
        }

        private void rbsabor1_CheckedChanged(object sender, EventArgs e)
        {
            cbsabor1.Enabled = true;
            cbsabor2.Enabled = false;
        }

        private void cbTamano1_SelectedIndexChanged(object sender, EventArgs e)
        {
            cbsabor1.Items.Clear();
            regist.busquedaxtamano(cbTamano1.Text, cbsabor1);
            cbsabor2.Items.Clear();
            regist.busquedaxtamano(cbTamano1.Text, cbsabor2);
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            cbsabor1.Enabled = true;
            cbsabor2.Enabled = true;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            res = true;
            cbTamano1.Items.Clear();
            this.Close();
        }
    }
}