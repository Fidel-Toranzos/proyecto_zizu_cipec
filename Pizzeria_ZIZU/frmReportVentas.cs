﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Pizzeria_ZIZU
{
    public partial class frmReportVentas : Form
    {
        public DateTime Fini;
        public DateTime Ffin;
        public frmReportVentas()
        {
            InitializeComponent();
        }

        private void frmReportVentas_Load(object sender, EventArgs e)
        {
            cReporteVenta ventamost = new cReporteVenta();
            ventamost.SetParameterValue("@fecha1", Fini);
            ventamost.SetParameterValue("@fecha2", Ffin);
            crystalReportViewer1.ReportSource = ventamost;
        }

        private void btnAtras_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
