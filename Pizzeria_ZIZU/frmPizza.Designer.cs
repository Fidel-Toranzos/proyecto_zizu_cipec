﻿namespace Pizzeria_ZIZU
{
    partial class frmPizza
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPizza));
            this.panel1 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.titulopiz = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.rbsabor1 = new System.Windows.Forms.RadioButton();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.rbyes = new System.Windows.Forms.RadioButton();
            this.rbno = new System.Windows.Forms.RadioButton();
            this.label4 = new System.Windows.Forms.Label();
            this.cbsabor2 = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cbsabor1 = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.cbTamano1 = new System.Windows.Forms.ComboBox();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.button2);
            this.panel1.Controls.Add(this.titulopiz);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(751, 654);
            this.panel1.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Constantia", 28F, System.Drawing.FontStyle.Bold);
            this.label2.ForeColor = System.Drawing.Color.OrangeRed;
            this.label2.Location = new System.Drawing.Point(251, 15);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(206, 58);
            this.label2.TabIndex = 47;
            this.label2.Text = "Pizza N°";
            // 
            // button2
            // 
            this.button2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.button2.BackColor = System.Drawing.Color.OrangeRed;
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.Font = new System.Drawing.Font("Century Gothic", 13F, System.Drawing.FontStyle.Bold);
            this.button2.ForeColor = System.Drawing.Color.White;
            this.button2.Location = new System.Drawing.Point(406, 540);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(280, 96);
            this.button2.TabIndex = 31;
            this.button2.Text = "Cancelar Pedido";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // titulopiz
            // 
            this.titulopiz.AutoSize = true;
            this.titulopiz.Font = new System.Drawing.Font("Constantia", 32F, System.Drawing.FontStyle.Bold);
            this.titulopiz.ForeColor = System.Drawing.Color.OrangeRed;
            this.titulopiz.Location = new System.Drawing.Point(446, 7);
            this.titulopiz.Name = "titulopiz";
            this.titulopiz.Size = new System.Drawing.Size(48, 66);
            this.titulopiz.TabIndex = 45;
            this.titulopiz.Text = "1";
            // 
            // button1
            // 
            this.button1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.button1.BackColor = System.Drawing.Color.OrangeRed;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.Font = new System.Drawing.Font("Century Gothic", 13F, System.Drawing.FontStyle.Bold);
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(67, 540);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(280, 96);
            this.button1.TabIndex = 31;
            this.button1.Text = "Aceptar";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(424, 96);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(310, 428);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 43;
            this.pictureBox1.TabStop = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.groupBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.groupBox1.Controls.Add(this.radioButton1);
            this.groupBox1.Controls.Add(this.rbsabor1);
            this.groupBox1.Controls.Add(this.panel2);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.cbsabor2);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.cbsabor1);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.cbTamano1);
            this.groupBox1.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(22, 96);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(386, 428);
            this.groupBox1.TabIndex = 42;
            this.groupBox1.TabStop = false;
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Font = new System.Drawing.Font("Constantia", 12F);
            this.radioButton1.Location = new System.Drawing.Point(157, 232);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(83, 28);
            this.radioButton1.TabIndex = 14;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "Elegir";
            this.radioButton1.UseVisualStyleBackColor = true;
            this.radioButton1.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            // 
            // rbsabor1
            // 
            this.rbsabor1.AutoSize = true;
            this.rbsabor1.Font = new System.Drawing.Font("Constantia", 12F);
            this.rbsabor1.Location = new System.Drawing.Point(155, 138);
            this.rbsabor1.Name = "rbsabor1";
            this.rbsabor1.Size = new System.Drawing.Size(83, 28);
            this.rbsabor1.TabIndex = 13;
            this.rbsabor1.Text = "Elegir";
            this.rbsabor1.UseVisualStyleBackColor = true;
            this.rbsabor1.CheckedChanged += new System.EventHandler(this.rbsabor1_CheckedChanged);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.rbyes);
            this.panel2.Controls.Add(this.rbno);
            this.panel2.Location = new System.Drawing.Point(6, 335);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(331, 51);
            this.panel2.TabIndex = 13;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Constantia", 16.2F);
            this.label5.Location = new System.Drawing.Point(14, 8);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(100, 35);
            this.label5.TabIndex = 15;
            this.label5.Text = "Borde :";
            // 
            // rbyes
            // 
            this.rbyes.AutoSize = true;
            this.rbyes.Font = new System.Drawing.Font("Constantia", 16.2F);
            this.rbyes.Location = new System.Drawing.Point(165, 4);
            this.rbyes.Name = "rbyes";
            this.rbyes.Size = new System.Drawing.Size(58, 39);
            this.rbyes.TabIndex = 16;
            this.rbyes.Text = "Si";
            this.rbyes.UseVisualStyleBackColor = true;
            // 
            // rbno
            // 
            this.rbno.AutoSize = true;
            this.rbno.Font = new System.Drawing.Font("Constantia", 16.2F);
            this.rbno.Location = new System.Drawing.Point(247, 5);
            this.rbno.Name = "rbno";
            this.rbno.Size = new System.Drawing.Size(72, 39);
            this.rbno.TabIndex = 17;
            this.rbno.Text = "No";
            this.rbno.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Constantia", 16.2F);
            this.label4.Location = new System.Drawing.Point(22, 228);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(118, 35);
            this.label4.TabIndex = 12;
            this.label4.Text = "Sabor 2 :";
            // 
            // cbsabor2
            // 
            this.cbsabor2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbsabor2.Font = new System.Drawing.Font("Constantia", 20F);
            this.cbsabor2.FormattingEnabled = true;
            this.cbsabor2.Location = new System.Drawing.Point(25, 270);
            this.cbsabor2.Name = "cbsabor2";
            this.cbsabor2.Size = new System.Drawing.Size(309, 48);
            this.cbsabor2.TabIndex = 11;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Constantia", 16.2F);
            this.label3.Location = new System.Drawing.Point(21, 132);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(113, 35);
            this.label3.TabIndex = 10;
            this.label3.Text = "Sabor 1 :";
            // 
            // cbsabor1
            // 
            this.cbsabor1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbsabor1.Font = new System.Drawing.Font("Constantia", 20F);
            this.cbsabor1.FormattingEnabled = true;
            this.cbsabor1.Location = new System.Drawing.Point(25, 175);
            this.cbsabor1.Name = "cbsabor1";
            this.cbsabor1.Size = new System.Drawing.Size(309, 48);
            this.cbsabor1.TabIndex = 9;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Constantia", 16.2F);
            this.label6.Location = new System.Drawing.Point(21, 31);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(124, 35);
            this.label6.TabIndex = 6;
            this.label6.Text = "Tamaño :";
            // 
            // cbTamano1
            // 
            this.cbTamano1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbTamano1.Font = new System.Drawing.Font("Constantia", 20F);
            this.cbTamano1.FormattingEnabled = true;
            this.cbTamano1.Location = new System.Drawing.Point(28, 73);
            this.cbTamano1.Name = "cbTamano1";
            this.cbTamano1.Size = new System.Drawing.Size(309, 48);
            this.cbTamano1.TabIndex = 5;
            this.cbTamano1.SelectedIndexChanged += new System.EventHandler(this.cbTamano1_SelectedIndexChanged);
            // 
            // frmPizza
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(751, 654);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "frmPizza";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "frmPizza";
            this.Load += new System.EventHandler(this.frmPizza_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cbTamano1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cbsabor2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cbsabor1;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.RadioButton rbsabor1;
        public System.Windows.Forms.Label titulopiz;
        public System.Windows.Forms.Button button1;
        public System.Windows.Forms.Button button2;
        public System.Windows.Forms.Label label2;
        private System.Windows.Forms.RadioButton rbno;
        private System.Windows.Forms.RadioButton rbyes;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel panel2;
    }
}