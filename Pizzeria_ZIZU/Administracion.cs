﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Control;
using System.Windows.Forms;

namespace Pizzeria_ZIZU
{
    public partial class Administracion : Form
    {
        //variable para instanciar las funciones de la clase formcall control
        private FormCall vent = new FormCall();
        public Administracion()
        {
            InitializeComponent();
        }
        private void imgProduc_Click_1(object sender, EventArgs e)
        {
            subMenuProduc.Visible = true;
        }

        private void btnAgregarProduc_Click_1(object sender, EventArgs e)
        {
            subMenuProduc.Visible = false;
            vent.abrirForm(new Agregar_pizza(), panelEscritorio, lblTitle);

        }

        private void btnModificarProduc_Click_1(object sender, EventArgs e)
        {
            subMenuProduc.Visible = false;
            vent.abrirForm(new Modificar_pizza(), panelEscritorio, lblTitle);
        }

        private void btnEliminarProduc_Click_1(object sender, EventArgs e)
        {
            subMenuProduc.Visible = false;
            vent.abrirForm(new Eliminar_pizza(), panelEscritorio, lblTitle);
        }

        

        private void subMenuProduc_Paint(object sender, PaintEventArgs e)
        {

        }
        private void Administracion_Load(object sender, EventArgs e)
        {

        }

       

        private void btnAtrass_Click(object sender, EventArgs e)
        {
            this.Close();
          
        }

      
    }
}
