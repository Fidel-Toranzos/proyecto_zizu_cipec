﻿namespace Pizzeria_ZIZU
{
    partial class Administracion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelEscritorio = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.lblTitle = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.imgProduc = new FontAwesome.Sharp.IconPictureBox();
            this.subMenuProduc = new System.Windows.Forms.Panel();
            this.btnEliminarProduc = new FontAwesome.Sharp.IconButton();
            this.btnModificarProduc = new FontAwesome.Sharp.IconButton();
            this.btnAgregarProduc = new FontAwesome.Sharp.IconButton();
            this.btnAtrass = new FontAwesome.Sharp.IconButton();
            this.panelEscritorio.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgProduc)).BeginInit();
            this.subMenuProduc.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelEscritorio
            // 
            this.panelEscritorio.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.panelEscritorio.Controls.Add(this.label2);
            this.panelEscritorio.Controls.Add(this.lblTitle);
            this.panelEscritorio.Controls.Add(this.label18);
            this.panelEscritorio.Controls.Add(this.imgProduc);
            this.panelEscritorio.Controls.Add(this.subMenuProduc);
            this.panelEscritorio.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelEscritorio.Location = new System.Drawing.Point(0, 0);
            this.panelEscritorio.Name = "panelEscritorio";
            this.panelEscritorio.Size = new System.Drawing.Size(1674, 991);
            this.panelEscritorio.TabIndex = 51;
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Freestyle Script", 65.8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))));
            this.label2.Location = new System.Drawing.Point(598, 108);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(454, 131);
            this.label2.TabIndex = 53;
            this.label2.Text = "Administracion";
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.Location = new System.Drawing.Point(13, 13);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(0, 17);
            this.lblTitle.TabIndex = 52;
            // 
            // label18
            // 
            this.label18.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Constantia", 25.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(157, 343);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(212, 53);
            this.label18.TabIndex = 51;
            this.label18.Text = "Producto";
            // 
            // imgProduc
            // 
            this.imgProduc.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.imgProduc.BackColor = System.Drawing.Color.DarkOrange;
            this.imgProduc.ForeColor = System.Drawing.SystemColors.ControlText;
            this.imgProduc.IconChar = FontAwesome.Sharp.IconChar.PizzaSlice;
            this.imgProduc.IconColor = System.Drawing.SystemColors.ControlText;
            this.imgProduc.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.imgProduc.IconSize = 401;
            this.imgProduc.Location = new System.Drawing.Point(84, 420);
            this.imgProduc.Name = "imgProduc";
            this.imgProduc.Size = new System.Drawing.Size(401, 403);
            this.imgProduc.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgProduc.TabIndex = 49;
            this.imgProduc.TabStop = false;
            this.imgProduc.Click += new System.EventHandler(this.imgProduc_Click_1);
            // 
            // subMenuProduc
            // 
            this.subMenuProduc.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.subMenuProduc.BackColor = System.Drawing.Color.Orange;
            this.subMenuProduc.Controls.Add(this.btnEliminarProduc);
            this.subMenuProduc.Controls.Add(this.btnModificarProduc);
            this.subMenuProduc.Controls.Add(this.btnAgregarProduc);
            this.subMenuProduc.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.subMenuProduc.Location = new System.Drawing.Point(488, 537);
            this.subMenuProduc.Name = "subMenuProduc";
            this.subMenuProduc.Size = new System.Drawing.Size(255, 200);
            this.subMenuProduc.TabIndex = 48;
            this.subMenuProduc.Visible = false;
            this.subMenuProduc.Paint += new System.Windows.Forms.PaintEventHandler(this.subMenuProduc_Paint);
            // 
            // btnEliminarProduc
            // 
            this.btnEliminarProduc.FlatAppearance.BorderSize = 0;
            this.btnEliminarProduc.FlatAppearance.MouseOverBackColor = System.Drawing.Color.OrangeRed;
            this.btnEliminarProduc.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEliminarProduc.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.btnEliminarProduc.Font = new System.Drawing.Font("Constantia", 13F);
            this.btnEliminarProduc.ForeColor = System.Drawing.Color.Black;
            this.btnEliminarProduc.IconChar = FontAwesome.Sharp.IconChar.Eraser;
            this.btnEliminarProduc.IconColor = System.Drawing.Color.Black;
            this.btnEliminarProduc.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.btnEliminarProduc.IconSize = 32;
            this.btnEliminarProduc.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEliminarProduc.Location = new System.Drawing.Point(5, 140);
            this.btnEliminarProduc.Name = "btnEliminarProduc";
            this.btnEliminarProduc.Padding = new System.Windows.Forms.Padding(10, 0, 20, 0);
            this.btnEliminarProduc.Rotation = 0D;
            this.btnEliminarProduc.Size = new System.Drawing.Size(271, 60);
            this.btnEliminarProduc.TabIndex = 3;
            this.btnEliminarProduc.Text = "Eliminar Producto";
            this.btnEliminarProduc.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEliminarProduc.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnEliminarProduc.UseVisualStyleBackColor = true;
            this.btnEliminarProduc.Click += new System.EventHandler(this.btnEliminarProduc_Click_1);
            // 
            // btnModificarProduc
            // 
            this.btnModificarProduc.FlatAppearance.BorderSize = 0;
            this.btnModificarProduc.FlatAppearance.MouseOverBackColor = System.Drawing.Color.OrangeRed;
            this.btnModificarProduc.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnModificarProduc.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.btnModificarProduc.Font = new System.Drawing.Font("Constantia", 13F);
            this.btnModificarProduc.ForeColor = System.Drawing.Color.Black;
            this.btnModificarProduc.IconChar = FontAwesome.Sharp.IconChar.Cog;
            this.btnModificarProduc.IconColor = System.Drawing.Color.Black;
            this.btnModificarProduc.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.btnModificarProduc.IconSize = 32;
            this.btnModificarProduc.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnModificarProduc.Location = new System.Drawing.Point(5, 70);
            this.btnModificarProduc.Name = "btnModificarProduc";
            this.btnModificarProduc.Padding = new System.Windows.Forms.Padding(10, 0, 20, 0);
            this.btnModificarProduc.Rotation = 0D;
            this.btnModificarProduc.Size = new System.Drawing.Size(271, 64);
            this.btnModificarProduc.TabIndex = 2;
            this.btnModificarProduc.Text = "Modificar Producto";
            this.btnModificarProduc.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnModificarProduc.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnModificarProduc.UseVisualStyleBackColor = true;
            this.btnModificarProduc.Click += new System.EventHandler(this.btnModificarProduc_Click_1);
            // 
            // btnAgregarProduc
            // 
            this.btnAgregarProduc.FlatAppearance.BorderSize = 0;
            this.btnAgregarProduc.FlatAppearance.MouseOverBackColor = System.Drawing.Color.OrangeRed;
            this.btnAgregarProduc.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAgregarProduc.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.btnAgregarProduc.Font = new System.Drawing.Font("Constantia", 13F);
            this.btnAgregarProduc.ForeColor = System.Drawing.Color.Black;
            this.btnAgregarProduc.IconChar = FontAwesome.Sharp.IconChar.PlusSquare;
            this.btnAgregarProduc.IconColor = System.Drawing.Color.Black;
            this.btnAgregarProduc.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.btnAgregarProduc.IconSize = 32;
            this.btnAgregarProduc.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAgregarProduc.Location = new System.Drawing.Point(5, 0);
            this.btnAgregarProduc.Name = "btnAgregarProduc";
            this.btnAgregarProduc.Padding = new System.Windows.Forms.Padding(10, 0, 20, 0);
            this.btnAgregarProduc.Rotation = 0D;
            this.btnAgregarProduc.Size = new System.Drawing.Size(250, 50);
            this.btnAgregarProduc.TabIndex = 1;
            this.btnAgregarProduc.Text = "Agregar Producto";
            this.btnAgregarProduc.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAgregarProduc.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnAgregarProduc.UseVisualStyleBackColor = true;
            this.btnAgregarProduc.Click += new System.EventHandler(this.btnAgregarProduc_Click_1);
            // 
            // btnAtrass
            // 
            this.btnAtrass.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAtrass.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btnAtrass.FlatAppearance.BorderSize = 0;
            this.btnAtrass.FlatAppearance.MouseOverBackColor = System.Drawing.Color.OrangeRed;
            this.btnAtrass.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAtrass.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.btnAtrass.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAtrass.ForeColor = System.Drawing.Color.Black;
            this.btnAtrass.IconChar = FontAwesome.Sharp.IconChar.FastBackward;
            this.btnAtrass.IconColor = System.Drawing.Color.Black;
            this.btnAtrass.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.btnAtrass.IconSize = 32;
            this.btnAtrass.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAtrass.Location = new System.Drawing.Point(1510, 2);
            this.btnAtrass.Name = "btnAtrass";
            this.btnAtrass.Padding = new System.Windows.Forms.Padding(10, 0, 20, 0);
            this.btnAtrass.Rotation = 0D;
            this.btnAtrass.Size = new System.Drawing.Size(160, 80);
            this.btnAtrass.TabIndex = 54;
            this.btnAtrass.Text = "Atras";
            this.btnAtrass.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAtrass.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnAtrass.UseVisualStyleBackColor = false;
            this.btnAtrass.Click += new System.EventHandler(this.btnAtrass_Click);
            // 
            // Administracion
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.Gold;
            this.ClientSize = new System.Drawing.Size(1674, 991);
            this.Controls.Add(this.btnAtrass);
            this.Controls.Add(this.panelEscritorio);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Administracion";
            this.Text = "Administracion";
            this.Load += new System.EventHandler(this.Administracion_Load);
            this.panelEscritorio.ResumeLayout(false);
            this.panelEscritorio.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgProduc)).EndInit();
            this.subMenuProduc.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panelEscritorio;
        private FontAwesome.Sharp.IconPictureBox imgProduc;
        private System.Windows.Forms.Panel subMenuProduc;
        private FontAwesome.Sharp.IconButton btnEliminarProduc;
        private FontAwesome.Sharp.IconButton btnModificarProduc;
        private FontAwesome.Sharp.IconButton btnAgregarProduc;
        private FontAwesome.Sharp.IconButton btnAtrass;
        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label18;
    }
}