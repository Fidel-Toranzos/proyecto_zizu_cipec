﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
//llamado capa control
using Control;
using BDSYSTEM;
using System.Data.SqlClient;

namespace Pizzeria_ZIZU
{
    public partial class Menu_Orde : Form
    {
        int idcl = 0;        
        ventaProceso otrped = new ventaProceso();
        Main_Menu Titulo = new Main_Menu();
        frmPizza pizza = new frmPizza();
        ConectionBD ver_red = new ConectionBD();
        DateTime fech = DateTime.Today;
        funcionPro pro = new funcionPro();
        public Menu_Orde()
        {
            InitializeComponent();
            cmbPizza.SelectedIndex = 0;
        }
        private void Menu_Orde_Load(object sender, EventArgs e)
        {
            txtNombre.Enabled = false;
            txtapellido.Enabled = false;
            txtdir.Enabled = false;
            txtelf.Enabled = false;
        }
        private void label11_Click(object sender, EventArgs e)
        {

        }
        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }
        private void btnAtras_Click(object sender, EventArgs e)
        {
            this.Close(); 
        }
    
        private void btnCrearPedido_Click(object sender, EventArgs e)
        {
            try
            {
                int c = Convert.ToInt32(cmbPizza.Text);
                if (cmbPizza.SelectedIndex != 0)
                {
                    listBox1.Items.Add("------------------------------------------");
                    listBox1.Items.Add("Registro de pedido N°" + Titulo.pedido);
                    listBox1.Items.Add("------------------------------------------");
                    listBox1.Items.Add("Cantidad de pizzas ordenadas : " + cmbPizza.Text);
                    for (int i = 1; i <= c; i++) //enviar como variable global 
                    {
                        pizza.titulopiz.Text = Convert.ToString(i);
                        listBox1.Items.Add("Orden N°" + i);
                        pizza.ShowDialog();
                        if (pizza.res == true)
                        {
                            cmbPizza.SelectedIndex = 0;
                            pizza.res = false;
                            pizza.tam1 = "";
                            listBox1.Items.Add("Pedido Cancelado");
                            MessageBox.Show("Pedido Cancelado");
                            i = c;
                            lbltotal.Text = "0 Bs";
                        }
                        else
                        {
                            if (i == 1)
                            {
                                listBox1.Items.Add("Tamaño de la pizza : " + pizza.tam1);
                                listBox1.Items.Add("Cantidad de sabor(s) : " + pizza.sabs1);
                                listBox1.Items.Add("Sabor pizza (1) : " + pizza.sabone1);
                                listBox1.Items.Add("Sabor pizza (2) : " + pizza.sabtwo1);
                                listBox1.Items.Add("Borda : " + pizza.borde1);
                                listBox1.Items.Add("Precio : " + pizza.precio1);
                            }
                            if (i == 2)
                            {
                                listBox1.Items.Add("Tamaño de la pizza : " + pizza.tam2);
                                listBox1.Items.Add("Cantidad de sabor(s) : " + pizza.sabs2);
                                listBox1.Items.Add("Sabor pizza (1) : " + pizza.sabone2);
                                listBox1.Items.Add("Sabor pizza (2) : " + pizza.sabtwo2);
                                listBox1.Items.Add("Borda : " + pizza.borde2);
                                listBox1.Items.Add("Precio : " + pizza.precio2);
                            }
                            if (i == 3)
                            {
                                listBox1.Items.Add("Tamaño de la pizza : " + pizza.tam3);
                                listBox1.Items.Add("Cantidad de sabor(s) : " + pizza.sabs3);
                                listBox1.Items.Add("Sabor pizza (1) : " + pizza.sabone3);
                                listBox1.Items.Add("Sabor pizza (2) : " + pizza.sabtwo3);
                                listBox1.Items.Add("Borda : " + pizza.borde3);
                                listBox1.Items.Add("Precio : " + pizza.precio3);
                            }
                            if (i == 4)
                            {
                                listBox1.Items.Add("Tamaño de la pizza : " + pizza.tam4);
                                listBox1.Items.Add("Cantidad de sabor(s) : " + pizza.sabs4);
                                listBox1.Items.Add("Sabor pizza (1) : " + pizza.sabone4);
                                listBox1.Items.Add("Sabor pizza (2) : " + pizza.sabtwo4);
                                listBox1.Items.Add("Borda : " + pizza.borde4);
                                listBox1.Items.Add("Precio : " + pizza.precio4);
                            }
                            if (i == 5)
                            {
                                listBox1.Items.Add("Tamaño de la pizza : " + pizza.tam5);
                                listBox1.Items.Add("Cantidad de sabor(s) : " + pizza.sabs5);
                                listBox1.Items.Add("Sabor pizza (1) : " + pizza.sabone5);
                                listBox1.Items.Add("Sabor pizza (2) : " + pizza.sabtwo5);
                                listBox1.Items.Add("Borda : " + pizza.borde5);
                                listBox1.Items.Add("Precio : " + pizza.precio5);
                            }
                            lbltotal.Text = Convert.ToString(pizza.total) + " Bs";
                        }
                        if (Convert.ToInt32(cmbPizza.Text) == i)
                        {
                            cmbPizza.Enabled = false;
                        }
                    }
                    btnCrearPedido.Enabled = false;
                }
                else
                {
                    MessageBox.Show("Ingrese una cantidad para realizar el pedido");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            txtNombre.Enabled = true;
            txtapellido.Enabled = true;
            txtelf.Enabled = false;
            txtdir.Enabled = false;
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            txtNombre.Enabled = true;
            txtapellido.Enabled = true;
            txtelf.Enabled = true;
            txtdir.Enabled = true;
        }

        private void txtNombre_KeyPress(object sender, KeyPressEventArgs e)
        {
            pro.caracteresSolo(e);
        }

        private void txtapellido_KeyPress(object sender, KeyPressEventArgs e)
        {
            pro.caracteresSolo(e);
        }

        private void txtelf_KeyPress(object sender, KeyPressEventArgs e)
        {
            pro.numerosSolo(e);
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            if (rblocal.Checked == true && rbdelivery.Checked == false)
            {
                if (cmbPizza.Text != "0")
                {
                    if (pizza.tam1 != "")
                    {
                        if (txtNombre.Text != "" && txtapellido.Text != "")
                        {
                            rblocal.Enabled = false;
                            rbdelivery.Enabled = false;
                            cmbPizza.Enabled = false;
                            btnCrearPedido.Enabled = false;
                            txtNombre.Enabled = false;
                            txtapellido.Enabled = false;
                            button1.Enabled = false;
                            idcl = Convert.ToInt32(otrped.jalaridcli());
                            otrped.registrarCliente(txtNombre.Text, txtapellido.Text, "null", 0);
                            ver_red.enlaceBD().Close();
                            for (int i = 1; i <= Convert.ToInt32(cmbPizza.Text); i++)
                            {
                                if (i == 1)
                                {
                                    if (pizza.sabs1 == "Dos")
                                    {
                                        otrped.registrarVentas((idcl + 1), pizza.sabone1 + "/" + pizza.sabtwo1, pizza.tam1, Convert.ToDecimal(pizza.precio1), Convert.ToDecimal(pizza.precioFabri1), pizza.borde1);
                                    }
                                    else
                                    {
                                        otrped.registrarVentas((idcl + 1), pizza.sabone1, pizza.tam1, Convert.ToDecimal(pizza.precio1), Convert.ToDecimal(pizza.precioFabri1), pizza.borde1);
                                    }
                                }
                                else if (i == 2)
                                {
                                    if (pizza.sabs2 == "Dos")
                                    {
                                        otrped.registrarVentas((idcl + 1), pizza.sabone2 + "/" + pizza.sabtwo2, pizza.tam2, Convert.ToDecimal(pizza.precio2), Convert.ToDecimal(pizza.precioFabri2), pizza.borde2);
                                    }
                                    else
                                    {
                                        otrped.registrarVentas((idcl + 1), pizza.sabone2, pizza.tam2, Convert.ToDecimal(pizza.precio2), Convert.ToDecimal(pizza.precioFabri2), pizza.borde2);
                                    }
                                }
                                else if (i == 3)
                                {
                                    if (pizza.sabs3 == "Dos")
                                    {
                                        otrped.registrarVentas((idcl + 1), pizza.sabone3 + "/" + pizza.sabtwo3, pizza.tam3, Convert.ToDecimal(pizza.precio3), Convert.ToDecimal(pizza.precioFabri3), pizza.borde3);
                                    }
                                    else
                                    {
                                        otrped.registrarVentas((idcl + 1), pizza.sabone3, pizza.tam3, Convert.ToDecimal(pizza.precio3), Convert.ToDecimal(pizza.precioFabri3), pizza.borde3);
                                    }
                                }
                                else if (i == 4)
                                {
                                    if (pizza.sabs4 == "Dos")
                                    {
                                        otrped.registrarVentas((idcl + 1), pizza.sabone4 + "/" + pizza.sabtwo4, pizza.tam4, Convert.ToDecimal(pizza.precio4), Convert.ToDecimal(pizza.precioFabri4), pizza.borde4);
                                    }
                                    else
                                    {
                                        otrped.registrarVentas((idcl + 1), pizza.sabone4, pizza.tam4, Convert.ToDecimal(pizza.precio4), Convert.ToDecimal(pizza.precioFabri4), pizza.borde4);
                                    }
                                }
                                else
                                {
                                    if (pizza.sabs5 == "Dos")
                                    {
                                        otrped.registrarVentas((idcl + 1), pizza.sabone5 + "/" + pizza.sabtwo5, pizza.tam5, Convert.ToDecimal(pizza.precio5), Convert.ToDecimal(pizza.precioFabri5), pizza.borde5);
                                    }
                                    else
                                    {
                                        otrped.registrarVentas((idcl + 1), pizza.sabone5, pizza.tam5, Convert.ToDecimal(pizza.precio5), Convert.ToDecimal(pizza.precioFabri5), pizza.borde5);
                                    }
                                }
                            }
                            ver_red.enlaceBD().Close();
                            MessageBox.Show("Se realizo el registro de manera satisfactoria");
                            Titulo.aumentoPedido();
                        }
                        else
                        {
                            MessageBox.Show("Debe llenar los datos del cliente");
                        }
                    }
                    else
                    {
                        MessageBox.Show("Ingrese los datos de los pedidos");
                    }
                }
                else
                {
                    MessageBox.Show("Debe selecionar la cantidad del pedido");
                }
            }
            else if (rblocal.Checked == false && rbdelivery.Checked == true)
            {
                if (cmbPizza.Text != "0")
                {
                    if (pizza.tam1 != "")
                    {
                        if (txtNombre.Text != "" && txtapellido.Text != "" && txtelf.Text != "" && txtelf.Text != "")
                        {
                            rblocal.Enabled = false;
                            rbdelivery.Enabled = false;
                            cmbPizza.Enabled = false;
                            btnCrearPedido.Enabled = false;
                            txtNombre.Enabled = false;
                            txtapellido.Enabled = false;
                            txtelf.Enabled = false;
                            txtdir.Enabled = false;
                            button1.Enabled = false;
                            otrped.registrarCliente(txtNombre.Text, txtapellido.Text, txtdir.Text, Convert.ToInt32(txtelf.Text));
                            idcl = Convert.ToInt32(otrped.jalaridcli());
                            ver_red.enlaceBD().Close();
                            for (int i = 1; i <= Convert.ToInt32(cmbPizza.Text); i++)
                            {
                                if (i == 1)
                                {
                                    if (pizza.sabs1 == "Dos")
                                    {
                                        otrped.registrarVentas((idcl + 1), pizza.sabone1 + "/" + pizza.sabtwo1, pizza.tam1, Convert.ToDecimal(pizza.precio1), Convert.ToDecimal(pizza.precioFabri1), pizza.borde1);
                                    }
                                    else
                                    {
                                        otrped.registrarVentas((idcl + 1), pizza.sabone1, pizza.tam1, Convert.ToDecimal(pizza.precio1), Convert.ToDecimal(pizza.precioFabri1), pizza.borde1);
                                    }
                                }
                                else if (i == 2)
                                {
                                    if (pizza.sabs2 == "Dos")
                                    {
                                        otrped.registrarVentas((idcl + 1), pizza.sabone2 + "/" + pizza.sabtwo2, pizza.tam2, Convert.ToDecimal(pizza.precio2), Convert.ToDecimal(pizza.precioFabri2), pizza.borde2);
                                    }
                                    else
                                    {
                                        otrped.registrarVentas((idcl + 1), pizza.sabone2, pizza.tam2, Convert.ToDecimal(pizza.precio2), Convert.ToDecimal(pizza.precioFabri2), pizza.borde2);
                                    }
                                }
                                else if (i == 3)
                                {
                                    if (pizza.sabs3 == "Dos")
                                    {
                                        otrped.registrarVentas((idcl + 1), pizza.sabone3 + "/" + pizza.sabtwo3, pizza.tam3, Convert.ToDecimal(pizza.precio3), Convert.ToDecimal(pizza.precioFabri3), pizza.borde3);
                                    }
                                    else
                                    {
                                        otrped.registrarVentas((idcl + 1), pizza.sabone3, pizza.tam3, Convert.ToDecimal(pizza.precio3), Convert.ToDecimal(pizza.precioFabri3), pizza.borde3);
                                    }
                                }
                                else if (i == 4)
                                {
                                    if (pizza.sabs4 == "Dos")
                                    {
                                        otrped.registrarVentas((idcl + 1), pizza.sabone4 + "/" + pizza.sabtwo4, pizza.tam4, Convert.ToDecimal(pizza.precio4), Convert.ToDecimal(pizza.precioFabri4), pizza.borde4);
                                    }
                                    else
                                    {
                                        otrped.registrarVentas((idcl + 1), pizza.sabone4, pizza.tam4, Convert.ToDecimal(pizza.precio4), Convert.ToDecimal(pizza.precioFabri4), pizza.borde4);
                                    }
                                }
                                else
                                {
                                    if (pizza.sabs5 == "Dos")
                                    {
                                        otrped.registrarVentas((idcl + 1), pizza.sabone5 + "/" + pizza.sabtwo5, pizza.tam5, Convert.ToDecimal(pizza.precio5), Convert.ToDecimal(pizza.precioFabri5), pizza.borde5);
                                    }
                                    else
                                    {
                                        otrped.registrarVentas((idcl + 1), pizza.sabone5, pizza.tam5, Convert.ToDecimal(pizza.precio5), Convert.ToDecimal(pizza.precioFabri5), pizza.borde5);
                                    }
                                }
                            }
                            ver_red.enlaceBD().Close();
                            MessageBox.Show("Se realizo el registro de manera satisfactoria");
                            Titulo.aumentoPedido();
                        }
                        else
                        {
                            MessageBox.Show("Debe llenar los datos del cliente");
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Debe selecionar la cantidad del pedido");
                }
            }
            else
            {
                MessageBox.Show("Selecione una opcion en Pedido");
            }
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            if (button1.Enabled == false)
            {
                MessageBox.Show("Listo para realizar una nueva venta");
                listBox1.Items.Clear();
                listBox1.Items.Add("Sistema de gestor de pedidos -ZIZU-");
                rblocal.Enabled = true;
                rblocal.Checked = false;
                rbdelivery.Enabled = true;
                rbdelivery.Checked = false;
                cmbPizza.Enabled = true;
                cmbPizza.SelectedIndex = 0;
                btnCrearPedido.Enabled = true;
                txtNombre.Text = null;
                txtNombre.Enabled = false;
                txtapellido.Text = null;
                txtapellido.Enabled = false;
                txtelf.Text = null;
                txtelf.Enabled = false;
                txtdir.Text = null;
                txtdir.Enabled = false;
                button1.Enabled = true;
                lbltotal.Text = "0 Bs";
                pizza.total = 0;
            }
            else
            {
                MessageBox.Show("Debe registrar la venta primeramente");
            }
        }
        private void btnLimpiar_Click_1(object sender, EventArgs e)
        {
            if ((rblocal.Checked == false && rbdelivery.Checked == false) && cmbPizza.Text == "0")
            {
                MessageBox.Show("No hay pedido por cancelar");
                listBox1.Items.Clear();
                listBox1.Items.Add("Sistema de gestor de pedidos -ZIZU-");
                btnCrearPedido.Enabled = true;
            }
            else if (button1.Enabled == false)
            {
                MessageBox.Show("El pedido ya fue registrado, no se puede limpiar el registro");
            }
            else
            {
                MessageBox.Show("Pedido de registro de venta cancelado");
                listBox1.Items.Clear();
                listBox1.Items.Add("Sistema de gestor de pedidos -ZIZU-");
                rblocal.Enabled = true;
                rblocal.Checked = false;
                rbdelivery.Enabled = true;
                rbdelivery.Checked = false;
                cmbPizza.Enabled = true;
                cmbPizza.SelectedIndex = 0;
                btnCrearPedido.Enabled = true;
                txtNombre.Text = null;
                txtNombre.Enabled = false;
                txtapellido.Text = null;
                txtapellido.Enabled = false;
                txtelf.Text = null;
                txtelf.Enabled = false;
                txtdir.Text = null;
                txtdir.Enabled = false;
                button1.Enabled = true;
                lbltotal.Text = "0 Bs";
                pizza.total = 0;
            }
        }
    }
}
