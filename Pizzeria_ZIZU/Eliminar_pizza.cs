﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using BDSYSTEM;
using Control;
using System.Data.SqlClient;

namespace Pizzeria_ZIZU
{
    public partial class Eliminar_pizza : Form
    {
        Proceso proce_con = new Proceso();
        ConectionBD contbd = new ConectionBD();
        int posicion;
        public Eliminar_pizza()
        {
            InitializeComponent();
            proce_con.actualizar(dataGridView1);
            contbd.enlaceBD().Close();
        }

        private void btnAtras_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void btneliminar_Click(object sender, EventArgs e) //Boton eliminar
        {
            try
            {
                if (lblidpizza.Text != "None")
                {
                    if (MessageBox.Show("¿Desea eliminar los siguientes datos?\nNombre Pizza: "+ dataGridView1[1, posicion].Value.ToString() +"\nTamaño Pizza: "+ dataGridView1[2, posicion].Value.ToString() + "\nPrecio venta Pizza: " + dataGridView1[3, posicion].Value.ToString() + "\nPrecio fabricacion Pizza: " + dataGridView1[4, posicion].Value.ToString() + "\nDescripcion Pizza: " + dataGridView1[5, posicion].Value.ToString(), "Mensaje", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        if (proce_con.eliminiarint(Convert.ToInt32(lblidpizza.Text)) == 1)
                        {
                            lblidpizza.Text = "None";
                            proce_con.actualizar(dataGridView1);
                            contbd.enlaceBD().Close();
                            MessageBox.Show("Datos eliminados");
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Debe selecionar una columna para poder eliminarlo");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button1_Click(object sender, EventArgs e) // Boton seleccionar opcion
        {
            if (proce_con.controlVentanaActualizar(dataGridView1))
            {
                posicion = dataGridView1.CurrentRow.Index;
                lblidpizza.Text = dataGridView1[0, posicion].Value.ToString();
            }
            else
            {
                MessageBox.Show("Debe selecionar un dato de la tabla");
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            posicion = dataGridView1.CurrentRow.Index;
            lblidpizza.Text = dataGridView1[0, posicion].Value.ToString();
        }
    }
}
