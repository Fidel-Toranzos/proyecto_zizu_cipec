﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Control;

namespace Pizzeria_ZIZU
{
  
    public partial class frmFechas : Form
    {
        FormCall llamadoForm = new FormCall();
        public frmFechas()
        {
            InitializeComponent();
        }

        private void reportePrueba_Load(object sender, EventArgs e)
        {
            this.CenterToScreen();
        }

      
        private void btnGeneral_Click_1(object sender, EventArgs e)
        {
            try
            {                    
                DateTime fecini = new DateTime();
                string dato1 = dtpFechaInicial.Text;
                fecini = DateTime.Parse(dato1);
                DateTime fecfin = new DateTime();
                string dato2 = dpFechaFinal.Text;
                fecfin = DateTime.Parse(dato2);
                fecini = Convert.ToDateTime(dtpFechaInicial.Text);
                fecfin = Convert.ToDateTime(dpFechaFinal.Text);
                int res = fecini.CompareTo(fecfin);
                if (res > 0)
                {
                    MessageBox.Show("Error en el ingreso de las fechas");
                }
                else
                {
                    frmReportVentas repote = new frmReportVentas();
                    repote.Fini = Convert.ToDateTime(fecini.ToString("yyyy-MM-dd"));
                    repote.Ffin = Convert.ToDateTime(fecfin.ToString("yyyy-MM-dd"));
                    llamadoForm.abrirForm(repote, panelEscritorio, lblTitle);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void btnAtras_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dtpFechaInicial_ValueChanged(object sender, EventArgs e)
        {

        }
        private void dpFechaFinal_ValueChanged(object sender, EventArgs e)
        {

        }
        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
