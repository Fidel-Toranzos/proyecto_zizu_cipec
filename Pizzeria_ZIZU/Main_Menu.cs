﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
//se Importo la libreria para los iconos
using FontAwesome.Sharp;
using System.Drawing.Text;
using Control;

namespace Pizzeria_ZIZU
{
    
    public partial class Main_Menu : Form
    {
        //Declaro campos para almacenar el boton
        Loading_Bar invi = new Loading_Bar();
        public int pedido = 1;
        private IconButton actualBtn;
        private Panel leftBorderBtn;
        FormCall llamadoForm = new FormCall();
        public Main_Menu()
        {
            InitializeComponent();
            leftBorderBtn = new Panel();
            leftBorderBtn.Size = new Size(7, 60);
            panelMenu.Controls.Add(leftBorderBtn);
            //Limites de la ventana del FORMULARIO
            this.Text = string.Empty;
            this.ControlBox = false;
            this.DoubleBuffered = true;
            if (invi.Visible == false)
                invi.Close();
           // this.MaximizedBounds = Screen.FromHandle(this.Handle).WorkingArea;
        }
        //mover,redimencionar el Formulario
      /*  [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();

        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hWnd, int wMsg, int wParam, int lParam);*/
        //End Metodo redimencion frm
        //Metodo para activar el resalatar el boton
        private void ActivateButton(object senderBtn, Color color)
        {
            if (senderBtn != null)
            {
                DisableButton();
                actualBtn = (IconButton)senderBtn;
                actualBtn.BackColor = Color.Red;
                //actualBtn.BackColor = Color.FromArgb(255, 128, 0);
                actualBtn.ForeColor = color;
                actualBtn.TextAlign = ContentAlignment.MiddleCenter;
                actualBtn.IconColor = color;
                actualBtn.TextImageRelation = TextImageRelation.TextBeforeImage;
                actualBtn.ImageAlign = ContentAlignment.MiddleRight;
                //leftborderbtn
                leftBorderBtn.BackColor = color;
                leftBorderBtn.Location = new Point(0, actualBtn.Location.Y);
                leftBorderBtn.Visible = true;
                leftBorderBtn.BringToFront();
                //acualtual ser igual a lbltitle
                iconActualArriba.IconChar = actualBtn.IconChar;
                iconActualArriba.IconColor = color;
            }
        }
        //END Metodo resaltar boton

        //Metodo para Desactivar el resaltado del boton
        private void DisableButton()
        {
            if (actualBtn != null)
            {
                actualBtn.BackColor = Color.FromArgb(64, 64, 64);
                // actualBtn.BackColor = Color.FromArgb(255, 128, 0);
                actualBtn.ForeColor = Color.White;
                actualBtn.TextAlign = ContentAlignment.MiddleLeft;
                actualBtn.IconColor = Color.White;
                actualBtn.TextImageRelation = TextImageRelation.ImageBeforeText;
                actualBtn.ImageAlign = ContentAlignment.MiddleLeft;
            }
        }
        //END Metodo Desactivar boton
        //para cuando le den Click al logo este reinicio el resaltado en el MENU
        private void Reset()
        {
            DisableButton();
            leftBorderBtn.Visible = false;
            iconActualArriba.IconChar = IconChar.Home;
            iconActualArriba.IconColor = Color.Yellow;
            lblTitle.Text = "Inicio";
        }
        //End
        private void btnAdm_Click(object sender, EventArgs e)
        {
            subMenuReport.Visible = false;
            ActivateButton(sender, Color.White);
            llamadoForm.abrirForm(new Administracion(), panelEscritorio, lblTitle);     
        }
        private void btnVentas_Click(object sender, EventArgs e)
        {
            ActivateButton(sender, Color.White);
            llamadoForm.abrirForm(new Menu_Orde(), panelEscritorio, lblTitle);
            subMenuReport.Visible = false;
        }
        private void btnReportes_Click(object sender, EventArgs e)
        {
            llamadoForm.cerrarForm();
            ActivateButton(sender, Color.White);
            subMenuReport.Visible = true;
            //frmOpciones option = new frmOpciones();
           // option.ShowDialog();
           // llamadoForm.abrirForm2(new frmOpciones(), panelEscritorio);
        
        }
        private void btnAyuda_Click(object sender, EventArgs e)
        {
            ActivateButton(sender, Color.White);
            llamadoForm.abrirForm(new Ayuda(), panelEscritorio, lblTitle);
            subMenuReport.Visible = false;

        }
        private void btnAcerca_Click(object sender, EventArgs e)
        {
            ActivateButton(sender, Color.White);
            llamadoForm.abrirForm(new Acerca_De(), panelEscritorio, lblTitle);
            subMenuReport.Visible = false;

        }      
        private void panelTitleBar_MouseDown(object sender, MouseEventArgs e)
        {
          //  ReleaseCapture();
          // SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            Application.Exit();
            Login ventanauser = new Login();
            ventanauser.Visible = true;
        }
        private void btnMinimizar_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
           
        }
        private void btnMax_Click_1(object sender, EventArgs e)
        {
            /*if (WindowState == FormWindowState.Normal)
            {
                WindowState = FormWindowState.Maximized;
            }
            else
            {
                WindowState = FormWindowState.Normal;
            }*/
        }
        private void btnLogo_Click(object sender, EventArgs e)
        {
            llamadoForm.cerrarForm();
            Reset();
            subMenuReport.Visible = false;
            //subMenuReport.Visible = false;
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        private void Main_Menu_Load(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Normal;
            lblfecha.Parent = pictureBox1;
            lblfecha.BackColor = Color.Transparent;
            lblfecha.Text = "Fecha: " + DateTime.Today.ToString("dd/MM/yyyy");
        }
        private void panelEscritorio_Paint(object sender, PaintEventArgs e)
        {
          
        }
        private void btnReportProduc_Click_1(object sender, EventArgs e)
        {
            frmReportProduc prodnew = new frmReportProduc();
            prodnew.Show();
            llamadoForm.abrirForm(prodnew, panelEscritorio, lblTitle);
            // frmReportProduc hola = new frmReportProduc();
            // reportePrueba hola = new reportePrueba();
            //   hola.ShowDialog();
             subMenuReport.Visible = false;
        }

        private void btnReportVent_Click_1(object sender, EventArgs e)
        {
            llamadoForm.abrirForm(new frmFechas(), panelEscritorio, lblTitle);
             subMenuReport.Visible = false;
        }

        private void btnReportClient_Click(object sender, EventArgs e)
        {
            frmReportClient hola = new frmReportClient();
            hola.Show();
            llamadoForm.abrirForm(hola, panelEscritorio, lblTitle);
            subMenuReport.Visible = false;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            lblhora.Parent = pictureBox1;
            lblhora.BackColor = Color.Transparent;
            lblhora.Text = "Hora: " + DateTime.Now.ToLongTimeString();
        }
        public int aumentoPedido()
        {
            return pedido++;
        }
    }
}
