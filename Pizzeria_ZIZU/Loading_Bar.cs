﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace Pizzeria_ZIZU
{
    public partial class Loading_Bar : Form
    {
        Login var = new Login();
        public Loading_Bar()
        {
            InitializeComponent();
        }

        private void Loading_Bar_Load(object sender, EventArgs e)
        {
            timer1.Start();
            if (var.Visible == false)
                var.Close();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            progressBar1.Increment(3);
            if (progressBar1.Value == 3)
            {
                lblprogresobar.Text = "Cargando el sistema : Iniciando";
            }
            else if (progressBar1.Value == 15)
            {
                lblprogresobar.Text = "Cargando el sistema : DBZIZU";
            }
            else if (progressBar1.Value == 30)
            {
                lblprogresobar.Text = "Cargando el sistema : DBZIZU/Table";
            }
            else if (progressBar1.Value == 39)
            {
                lblprogresobar.Text = "Cargando el sistema : Pizzeria_ZIZU/DBZIZU/Table/Cliente";
            }
            else if (progressBar1.Value == 48)
            {
                lblprogresobar.Text = "Cargando el sistema : Pizzeria_ZIZU/DBZIZU/Table/Pizza";
            }
            else if (progressBar1.Value == 57)
            {
                lblprogresobar.Text = "Cargando el sistema : Pizzeria_ZIZU/DBZIZU/Table/Usuario";
            }
            else if (progressBar1.Value == 66)
            {
                lblprogresobar.Text = "Cargando el sistema : Pizzeria_ZIZU/DBZIZU/Table/Ventas";
            }
            else if (progressBar1.Value == 75)
            {
                lblprogresobar.Text = "Cargando el sistema : Pizzeria_ZIZU/DBSystem";
            }
            else if (progressBar1.Value == 84)
            {
                lblprogresobar.Text = "Cargando el sistema : Pizzeria_ZIZU/Control";
            }
            else if (progressBar1.Value == 93)
            {
                lblprogresobar.Text = "Cargando el sistema : Pizzeria_ZIZU/Interface";
            }
            else if (progressBar1.Value == 99)
            {
                lblprogresobar.Text = "Cargando el sistema : Pizzeria_ZIZU/Finalizado";
            }
            if (progressBar1.Value >= 100)
            {
                timer1.Stop();
                lblprogresobar.Text = "Cargando el sistema : Pizzeria_ZIZU";
                Main_Menu menuprincipal = new Main_Menu();
                this.Visible = false;
                menuprincipal.Show();
            }
        }

        private void progressBar1_Click(object sender, EventArgs e)
        {

        }
    }
}
