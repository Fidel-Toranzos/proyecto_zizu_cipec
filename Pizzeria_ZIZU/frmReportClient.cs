﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Pizzeria_ZIZU
{
    public partial class frmReportClient : Form
    {
        public frmReportClient()
        {
            InitializeComponent();
        }

        private void frmReportClient_Load(object sender, EventArgs e)
        {
            cReportClient clientes = new cReportClient();
            crystalReportViewer1.ReportSource = clientes;
        }

        private void btnAtras_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
