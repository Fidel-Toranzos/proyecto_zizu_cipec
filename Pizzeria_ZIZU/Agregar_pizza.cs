﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Data.SqlClient;
using System.Data.Sql;

using Control;
using BDSYSTEM;

namespace Pizzeria_ZIZU
{
    public partial class Agregar_pizza : Form
    {
        Proceso proce_control = new Proceso();
        ConectionBD ver_red = new ConectionBD();
        funcionPro pro = new funcionPro();
        public Agregar_pizza()
        {
            InitializeComponent();
            proce_control.actualizar(dataGridView1);
            ver_red.enlaceBD().Close();
        }
        private void btnAtras_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void button1_Click(object sender, EventArgs e) // Boton de agregar
        {
            try
            {
                if (txtnombre.Text != "" && comboBox1.Text != "" && txtventa.Text != "" && txtfabricacion.Text != "" && txtdescrip.Text != "")
                {                  
                    if (MessageBox.Show("¿Desea agregar los siguientes datos?\nNombre Pizza: " + txtnombre.Text + "\nTamaño Pizza: " + comboBox1.Text + "\nPrecio venta Pizza: " + txtventa.Text + "\nPrecio fabricacion Pizza: " + txtfabricacion.Text +"\nDescripcion Pizza: " + txtdescrip.Text, "Mensaje", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    { 
                        proce_control.registrar(txtnombre.Text, comboBox1.Text, Convert.ToDecimal(txtventa.Text), Convert.ToDecimal(txtfabricacion.Text), txtdescrip.Text);
                        txtnombre.Text = null;
                        comboBox1.Text = null;
                        txtventa.Text = null;
                        txtfabricacion.Text = null;
                        txtdescrip.Text = null;
                        proce_control.actualizar(dataGridView1);
                        ver_red.enlaceBD().Close();
                        MessageBox.Show("Datos registrados");
                    }
                }
                else
                {
                    MessageBox.Show("Debe ingresar nuevos datos");
                    txtnombre.Text = null;
                    comboBox1.Text = null;
                    txtventa.Text = null;
                    txtfabricacion.Text = null;
                    txtdescrip.Text = null;
                    txtnombre.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                txtnombre.Text = null;
                comboBox1.Text = null;
                txtventa.Text = null;
                txtfabricacion.Text = null;
                txtdescrip.Text = null;
                txtnombre.Focus();
            }
        }

        private void txtnombre_KeyPress(object sender, KeyPressEventArgs e)
        {
            pro.caracteresSolo(e);
        }

        private void txtventa_KeyPress(object sender, KeyPressEventArgs e)
        {
            pro.numeroyDecimal(sender, e);
        }

        private void txtfabricacion_KeyPress(object sender, KeyPressEventArgs e)
        {
            pro.numeroyDecimal(sender, e);
        }
    }
}
