﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Data.SqlClient;
using BDSYSTEM;
using Control;

namespace Pizzeria_ZIZU
{
    public partial class Modificar_pizza : Form
    {
        ConectionBD con = new ConectionBD();
        Proceso pro = new Proceso();
        funcionPro prov = new funcionPro();
        int posicion;
        public Modificar_pizza()
        {
            InitializeComponent();
            txtnombre.Enabled = false;
            comboBox1.Enabled = false;
            txtprecioventa.Enabled = false;
            txtpreciofabricacion.Enabled = false;
            txtdescripcion.Enabled = false;
            pro.actualizar(dataGridView1);
            con.enlaceBD().Close();
        }

        private void btnAtras_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void button2_Click(object sender, EventArgs e) //Boton modificar
        {
            try
            {
                if (txtnombre.Text != "")
                { 
                    if (MessageBox.Show("¿Desea modificar los siguientes datos?\nNombre Pizza: " + txtnombre.Text + "\nTamaño Pizza: " + comboBox1.Text + "\nPrecio venta Pizza: " + txtprecioventa.Text + "\nPrecio fabricacion Pizza: " + txtpreciofabricacion.Text + "\nDescripcion Pizza: " + txtdescripcion.Text, "Mensaje", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        if (txtnombre.Text.Equals(dataGridView1[1, posicion].Value.ToString()) && comboBox1.Text.Equals(dataGridView1[2, posicion].Value.ToString()) && txtprecioventa.Text.Equals(dataGridView1[3, posicion].Value.ToString()) && txtpreciofabricacion.Text.Equals(dataGridView1[4, posicion].Value.ToString()) && txtdescripcion.Text.Equals(dataGridView1[5, posicion].Value.ToString()))
                        {
                            MessageBox.Show("Modifique algun valor para poder realizar esta accion");
                        }     
                        else if (pro.modicarint(txtnombre.Text, comboBox1.Text, Convert.ToDecimal(txtprecioventa.Text), Convert.ToDecimal(txtpreciofabricacion.Text), txtdescripcion.Text, Convert.ToInt32(lblid.Text)) == 1)
                        {
                            lblid.Text = "None";
                            txtnombre.Enabled = false;
                            comboBox1.Enabled = false;
                            txtprecioventa.Enabled = false;
                            txtpreciofabricacion.Enabled = false;
                            txtdescripcion.Enabled = false;
                            txtnombre.Text = null;
                            comboBox1.Text = null;
                            txtprecioventa.Text = null;
                            txtpreciofabricacion.Text = null;
                            txtdescripcion.Text = null;
                            txtnombre.Focus();
                            pro.actualizar(dataGridView1);
                            con.enlaceBD().Close();
                            MessageBox.Show("Se cambio el dato");
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Debe seleccionar la columna para modificar");
                    lblid.Text = "None";
                    txtnombre.Enabled = false;
                    comboBox1.Enabled = false;
                    txtprecioventa.Enabled = false;
                    txtpreciofabricacion.Enabled = false;
                    txtdescripcion.Enabled = false;
                    txtnombre.Text = null;
                    comboBox1.Text = null;
                    txtprecioventa.Text = null;
                    txtpreciofabricacion.Text = null;
                    txtdescripcion.Text = null;
                    txtnombre.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                lblid.Text = "None";
                txtnombre.Enabled = false;
                comboBox1.Enabled = false;
                txtprecioventa.Enabled = false;
                txtpreciofabricacion.Enabled = false;
                txtdescripcion.Enabled = false;
                txtnombre.Text = null;
                comboBox1.Text = null;
                txtprecioventa.Text = null;
                txtpreciofabricacion.Text = null;
                txtdescripcion.Text = null;
                txtnombre.Focus();
            }
        }
        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            posicion = dataGridView1.CurrentRow.Index;
            lblid.Text = dataGridView1[0, posicion].Value.ToString();
            txtnombre.Text = dataGridView1[1, posicion].Value.ToString();
            comboBox1.Text = dataGridView1[2, posicion].Value.ToString();
            txtprecioventa.Text = dataGridView1[3, posicion].Value.ToString();
            txtpreciofabricacion.Text = dataGridView1[4, posicion].Value.ToString();
            txtdescripcion.Text = dataGridView1[5, posicion].Value.ToString();
            txtnombre.Enabled = true;
            txtnombre.Enabled = true;
            comboBox1.Enabled = true;
            txtprecioventa.Enabled = true;
            txtpreciofabricacion.Enabled = true;
            txtdescripcion.Enabled = true;
        }

        private void button1_Click(object sender, EventArgs e) // Boton seleccionar
        {
            if (pro.controlVentanaActualizar(dataGridView1))
            {
                int posicion = dataGridView1.CurrentRow.Index;
                lblid.Text = dataGridView1[0, posicion].Value.ToString(); 
                txtnombre.Text = dataGridView1[1, posicion].Value.ToString();
                comboBox1.Text = dataGridView1[2, posicion].Value.ToString();
                txtprecioventa.Text = dataGridView1[3, posicion].Value.ToString();
                txtpreciofabricacion.Text = dataGridView1[4, posicion].Value.ToString();
                txtdescripcion.Text = dataGridView1[5, posicion].Value.ToString();
                txtnombre.Enabled = true;
                txtnombre.Enabled = true;
                comboBox1.Enabled = true;
                txtprecioventa.Enabled = true;
                txtpreciofabricacion.Enabled = true;
                txtdescripcion.Enabled = true;
            }
            else
            {
                MessageBox.Show("Debe selecionar un dato de la tabla");
            }
        }

        private void txtnombre_KeyPress(object sender, KeyPressEventArgs e)
        {
            prov.caracteresSolo(e);
        }

        private void txtprecioventa_KeyPress(object sender, KeyPressEventArgs e)
        {
            prov.numeroyDecimal(sender, e);
        }
    }
}
