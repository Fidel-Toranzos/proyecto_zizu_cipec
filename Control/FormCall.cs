﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;  //libreia para llamar a los metodos de form

namespace Control
{
    public class FormCall
    {
        private Form frmHijoActual;
        public void abrirForm(Form frmHijo, Panel pnelHijo, Label title)
        {
            frmHijoActual = frmHijo;
            frmHijo.TopLevel = false;
            frmHijo.FormBorderStyle = FormBorderStyle.None;
            frmHijo.Dock = DockStyle.Fill;
            //para mostrar frm en el formulario principal
            pnelHijo.Controls.Add(frmHijo);
            pnelHijo.Tag = frmHijo;
            frmHijo.BringToFront(); // trae adelante
            frmHijo.Show();         //mostrar
            title.Text = frmHijo.Text;
        }
        public void abrirForm2(Form frmHijo, Panel pnelHijo)
        {
           /* frmHijoActual = frmHijo;
            frmHijo.TopLevel = false;
            frmHijo.FormBorderStyle = FormBorderStyle.None;
            frmHijo.Dock = DockStyle.Fill;*/
            //para mostrar frm en el formulario principal
           // pnelHijo.Controls.Add(frmHijo);
            pnelHijo.Tag = frmHijo;
            frmHijo.BringToFront(); // trae adelante
            frmHijo.ShowDialog();         //mostrar
           // title.Text = frmHijo.Text;
        }
        //END Metodo frm del centro

        public void cerrarForm()
        {
            if (frmHijoActual != null)
            {
                frmHijoActual.Close();
         
            }
        }
    }
}
