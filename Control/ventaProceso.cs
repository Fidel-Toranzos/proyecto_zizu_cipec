﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;
using System.Data.SqlClient;
using System.Data.Sql;
using System.Windows.Forms;

using BDSYSTEM;

namespace Control
{
    public class ventaProceso
    {
        ConectionBD ver_red = new ConectionBD();
        public string guardarDato(string dato)
        {
            string envio = dato;
            return envio;
        }
        public void busquedaxtamano(string tam, ComboBox sabor)
        {
            string query = "Select nombre_pizza from Pizza where tam_pizza = '" + tam + "'";
            SqlCommand comando = new SqlCommand(query, ver_red.enlaceBD());
            SqlDataReader register = comando.ExecuteReader();
            while (register.Read())
            {
                sabor.Items.Add(register["nombre_pizza"].ToString());
            }
            ver_red.enlaceBD().Close();
        }
        public string enviarPrecio(string tam, string sabor)
        {
            string res = null;
            string query = "Select pre_pizza_venta from Pizza where tam_pizza = '" + tam + "' and nombre_pizza = '" + sabor + "'";
            SqlCommand comando = new SqlCommand(query, ver_red.enlaceBD());
            SqlDataReader register = comando.ExecuteReader();
            while (register.Read())
            {
                res = Convert.ToString(register["pre_pizza_venta"]);
            }
            return res;
        }
        public string enviarFabricacion(string tam, string sabor)
        {
            string res = null;
            string query = "Select pre_pizza_fabricacion from Pizza where tam_pizza = '" + tam + "' and nombre_pizza = '" + sabor + "'";
            SqlCommand comando = new SqlCommand(query, ver_red.enlaceBD());
            SqlDataReader register = comando.ExecuteReader();
            while (register.Read())
            {
                res = Convert.ToString(register["pre_pizza_fabricacion"]);
            }
            return res;
        }
        public void registrarVentas(int cliente ,string nombre, string tama, decimal pVenta, decimal pFabri, string borde)
        {
            SqlConnection conexion = new SqlConnection(ver_red.conectionCadena());
            string cadena = "Insert into Ventas ([id_cliente],[nombre_pizza],[tama_piiza],[precio_venta],[precio_fabri], [borde], [fecha])  values ('" + cliente + "','" + nombre + "','" + tama + "','" + pVenta + "','" + pFabri + "','" + borde + "','" + DateTime.Today.ToString("yyyy-MM-dd") + "')";
            SqlCommand comando = new SqlCommand(cadena, ver_red.enlaceBD());
            comando.ExecuteNonQuery();
        }
        public void registrarCliente(string nombre, string apellido, string dir, int telf)
        {
            SqlConnection conexion = new SqlConnection(ver_red.conectionCadena());
            string cadena = "Insert into Cliente ([nombre_cli],[apellido_cli],[direccion_cli],[telefono_cli])  values ('" + nombre + "','" + apellido + "','" + dir + "','" + telf + "')";
            SqlCommand comando = new SqlCommand(cadena, ver_red.enlaceBD());
            comando.ExecuteNonQuery();
        }

        public string jalaridcli()  
        {
            string res = null;
            string query = "Select top(1) id_cli from Cliente order by id_cli desc";
            SqlCommand comando = new SqlCommand(query, ver_red.enlaceBD());
            SqlDataReader register = comando.ExecuteReader();
            while (register.Read())
            {
                res = Convert.ToString(register["id_cli"]);
            }
            return res;
        }
    }
}
