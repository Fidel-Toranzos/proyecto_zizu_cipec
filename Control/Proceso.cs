﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Data;
using System.Data.SqlClient;
using System.Data.Sql;

using BDSYSTEM;

namespace Control
{       
    public class Proceso
    {
        ConectionBD ver_red = new ConectionBD();
        public void registrar(string nombre, string tam, decimal venta, decimal fabricacion, string descrip)
        {
            SqlConnection redBD = new SqlConnection(ver_red.conectionCadena());
            string cadena = "Insert into Pizza ([nombre_pizza],[tam_pizza],[pre_pizza_venta],[pre_pizza_fabricacion],[des_pizza]) values ('" + nombre + "','" + tam + "','" + venta + "','" + fabricacion + "','" + descrip + "')";
            SqlCommand comando = new SqlCommand(cadena, ver_red.enlaceBD());
            comando.ExecuteNonQuery();
        }
        public void modificar(string nombre, string tam, decimal venta, decimal fabricacion, string descrip)
        {
            SqlConnection redBD = new SqlConnection(ver_red.conectionCadena());
            string cadena = "Update Pizza where ([nombre_pizza],[tam_pizza],[pre_pizza_venta],[pre_pizza_fabricacion],[des_pizza]) values ('" + nombre + "','" + tam + "','" + venta + "','" + fabricacion + "','" + descrip + "')";
            SqlCommand comando = new SqlCommand(cadena, ver_red.enlaceBD());
            comando.ExecuteNonQuery();
        }
        public void actualizar(DataGridView vertablas)
        {
            ConectionBD con = new ConectionBD();
            string query = "Select * from Pizza";
            SqlCommand comando = new SqlCommand(query, con.enlaceBD());
            SqlDataAdapter data = new SqlDataAdapter(comando);
            DataTable tabla = new DataTable();
            data.Fill(tabla);
            vertablas.DataSource = tabla;
        }
        public int modicarint(string nombre, string tam, decimal venta, decimal fabricacion, string descrip, int codigo)
        {
            int flag = 0;
            string cadena = "Update Pizza set nombre_pizza = '" + nombre + "', tam_pizza = '" + tam + "', pre_pizza_venta = '" + venta + "', pre_pizza_fabricacion = '" + fabricacion + "', des_pizza = '" + descrip + "' where id_pizza = '" + codigo + "'";
            SqlCommand comande = new SqlCommand(cadena, ver_red.enlaceBD());
            flag = comande.ExecuteNonQuery();
            return flag;
        }
        public int eliminiarint(int codigo)
        {
            int flag = 0;
            string cadena = "Delete from Pizza where id_pizza = '" + codigo + "'";
            SqlCommand comande = new SqlCommand(cadena, ver_red.enlaceBD());
            flag = comande.ExecuteNonQuery();
            return flag;
        }
        public bool controlVentanaActualizar(DataGridView ventana)
        {
            bool resp = true;
            if (ventana.Rows.Count == 0)
            {
                resp = false;
            }
            return resp;
        }
        public void busquedaxtamano (string tam, ComboBox sabor)
        {
            string query = "Select nombre_pizza from Pizza where tam_pizza = '" + tam + "'";
            SqlCommand comando = new SqlCommand(query, ver_red.enlaceBD());
            SqlDataReader register = comando.ExecuteReader();
            while (register.Read())
            {
                sabor.Items.Add(register["nombre_pizza"].ToString());
            }
            ver_red.enlaceBD().Close();
        }
    }
}
